import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import CreateForm from './views/CreateForm.vue'
import Manage from './views/Manage.vue'
import Search from './views/Search.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Search
    },
    {
      path: '/create',
      name: 'create',
      component: CreateForm
    },
    {
      path: '/manage',
      name: 'manage',
      component: Manage
    },
    {
      path: '/search',
      name: 'search',
      component: Search
    }
  ]
})


/*
path: '/manage',
      name: 'manage',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ /*'./views/About.vue')
      */
