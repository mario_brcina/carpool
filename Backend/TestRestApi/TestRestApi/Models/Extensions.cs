﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestRestApi.Models
{
    public static class Extensions
    {
        public static bool IsBetween(this DateTime dt1, DateTime dt2, DateTime dt3)
        {
            DateTime min = dt2 <= dt3 ? dt2 : dt3;
            DateTime max = dt2 >= dt3 ? dt2 : dt3;

            return dt1 >= min && dt1 <= max;
        }

        public static bool IsNotBetween(this DateTime dt1, DateTime dt2, DateTime dt3)
        {
            return !dt1.IsBetween(dt2, dt3);
        }
    }
}
