﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestRestApi.Models
{
    interface IRepository
    {
        List<Car> GetAllCars();
        List<Car> GetAvailbleCars(DateTime startDateTime, DateTime endDateTime);
        List<Car> GetAvailbleCars(DateTimeRange during);
        Car GetCar(string plates);

        List<Employee> GetEmployees();
        Employee GetEmployee(int id);
        List<Employee> GetAvailableEmployees(DateTime startDateTime, DateTime endDateTime);

        List<TravelPlan> GetTravelPlans();
        List<TravelPlan> GetTravelPlansInMonth(int month);
        TravelPlan GetTravelPlan(int id);

        void CreateTravelPlan(TravelPlan travelPlan);
        void CreateTravelPlan(DateTime startDateTime, DateTime endDateTime,
            string startLocation, string endLocation, Car car, 
            List<Employee> travelers);
        void EditTravelPlan(int id, TravelPlan newTravelPlan);
        void EditTravelPlan(int travelPlanId, DateTime startDateTime,
            DateTime endDateTime, string startLocation, string endLocation,
            Car car, List<Employee> travelers);
        void DeleteTravelPlan(int travelPlanId);


        /*****************************************/
        void CreateCar(Car car);

        List<Car> getUsedCars();
        List<Car> getUsedCars(int startYear, int startMonth, int startDay, int startHour, 
            int startMinutes, int endYear, int endMonth, int endDay, int endHour, int endMinutes);
    }
}
