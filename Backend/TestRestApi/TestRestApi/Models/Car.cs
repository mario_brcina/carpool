﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestRestApi.Models
{
    public class Car
    {
        public string Plates { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Color { get; set; }
        public int NumberOfSeats { get; set; }

        public Car(string plates, string name, string type,
            string color, int numberOfSeats)
        {
            this.Plates = plates;
            this.Name = name;
            this.Type = type;
            this.Color = color;
            this.NumberOfSeats = numberOfSeats;
        }

        public override string ToString()
        {
            return String.Format($"Plates: {Plates}, Name: {Name}, Type: {Type}, Color: {Color}, Number of seats: {NumberOfSeats}");
        }
    }
}
