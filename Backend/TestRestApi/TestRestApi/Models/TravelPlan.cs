﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestRestApi.Models
{
    public class TravelPlan
    {
        private static int id;
        public int Id { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public string StartLocation { get; set; }
        public string EndLocation { get; set; }
        public Car Car { get; set; }
        public List<Employee> Travelers { get; set; }

        public TravelPlan(DateTime startDateTime, DateTime endDateTime,
            string startLocation, string endLocation, Car car,
            List<Employee> travelers)
        {
            id += 1;
            this.Id = id;
            this.StartDateTime = startDateTime;
            this.EndDateTime = endDateTime;
            this.StartLocation = startLocation;
            this.EndLocation = endLocation;
            this.Car = car;
            this.Travelers = travelers;
        }

        public override string ToString()
        {
            string data = String.Format($"****\nTravelID: {Id}, \nStart time: {StartDateTime}, \nEnd time: {EndDateTime}, \nStart location: {StartLocation}, \nEnd location: {EndLocation}, \nCar:  {Car},\n");

            foreach (Employee emp in Travelers)
            {
                data += String.Format($"Employee: {emp}\n");
            }

            return data;
        }
    }
}
