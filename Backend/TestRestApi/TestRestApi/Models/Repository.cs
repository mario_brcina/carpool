﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestRestApi.Models
{
    public static class Repository
    {
        private static List<Car> Cars = SeedDB.InitCars();
        private static List<Employee> Employees = SeedDB.InitEmployees();
        private static List<TravelPlan> TravelPlans = new List<TravelPlan>(){
                new TravelPlan(new DateTime(2019, 7, 10, 12, 0, 0),
                    new DateTime(2019, 7, 11, 12, 0, 0), "Zagreb", "Vodnjan",
                    Repository.GetCar("ZG-100-PU"),
                    new List<Employee>() { Repository.GetEmployee(2), Repository.GetEmployee(3)}),

                new TravelPlan(new DateTime(2019, 7, 12, 12, 0, 0),
                    new DateTime(2019, 7, 13, 20, 0, 0), "Zagreb", "Osijek",
                    Repository.GetCar("ZG-101-PU"),
                    new List<Employee>() { Repository.GetEmployee(5), Repository.GetEmployee(6)}),

                new TravelPlan(new DateTime(2019, 7, 12, 12, 0, 0),
                    new DateTime(2019, 7, 13, 20, 0, 0), "Zagreb", "Rijeka",
                    Repository.GetCar("ZG-102-PU"),
                    new List<Employee>() { Repository.GetEmployee(2), Repository.GetEmployee(3)}),

                new TravelPlan(new DateTime(2019, 7, 12, 12, 0, 0),
                    new DateTime(2019, 7, 13, 20, 0, 0), "Zagreb", "Split",
                    Repository.GetCar("ZG-103-PU"),
                    new List<Employee>() { Repository.GetEmployee(23), Repository.GetEmployee(24)})


            };

        /*************************************************/

        /*private List<TravelPlan> _initTravelPlans()
        {
            return new List<TravelPlan>
            {
                new TravelPlan(new DateTime(2019, 7, 10, 12, 0, 0),
                    new DateTime(2019, 7, 11, 12, 0, 0), "Zagreb", "Vodnjan",
                    this.GetCar("ZG-100-PU"),
                    new List<Employee>() { this.GetEmployee(2), this.GetEmployee(3)}),

                new TravelPlan(new DateTime(2019, 7, 12, 12, 0, 0),
                    new DateTime(2019, 7, 13, 20, 0, 0), "Zagreb", "Osijek",
                    this.GetCar("ZG-101-PU"),
                    new List<Employee>() { this.GetEmployee(5), this.GetEmployee(6)}),

                new TravelPlan(new DateTime(2019, 7, 12, 12, 0, 0),
                    new DateTime(2019, 7, 13, 20, 0, 0), "Zagreb", "Rijeka",
                    this.GetCar("ZG-102-PU"),
                    new List<Employee>() { this.GetEmployee(2), this.GetEmployee(3)}),

                new TravelPlan(new DateTime(2019, 7, 12, 12, 0, 0),
                    new DateTime(2019, 7, 13, 20, 0, 0), "Zagreb", "Split",
                    this.GetCar("ZG-103-PU"),
                    new List<Employee>() { this.GetEmployee(23), this.GetEmployee(24)})


            };
        }*/

        /*************************************************/
        //Singleton
        //private static Repository instance;

        /*private Repository()
        {
            TravelPlans = _initTravelPlans();
        }

        public static Repository Instance {
            get {
                if (instance == null)
                {
                    instance = new Repository();
                }

                return instance;
            }
        }*/

        public static void CreateTravelPlan(TravelPlan travelPlan)
        {
            TravelPlans.Add(travelPlan);
        }

        public static void CreateTravelPlan(DateTime startDateTime, DateTime endDateTime, string startLocation, string endLocation, Car car, List<Employee> travelers)
        {
            // validate
            TravelPlans.Add(new TravelPlan(startDateTime, endDateTime, startLocation,
                endLocation, car, travelers));
        }

        public static void DeleteTravelPlan(int travelPlanId)
        {
            //TravelPlan plan = TravelPlans.Find(p => p.Id.Equals(travelPlanId));
            TravelPlans.Remove(TravelPlans.Find(p => p.Id == travelPlanId));
        }

        public static void EditTravelPlan(int id, TravelPlan newTravelPlan)
        {
            TravelPlan plan = TravelPlans.Find(p => p.Id.Equals(id));

            plan.StartDateTime = newTravelPlan.StartDateTime;
            plan.EndDateTime = newTravelPlan.EndDateTime;
            plan.StartLocation = newTravelPlan.StartLocation;
            plan.EndLocation = newTravelPlan.EndLocation;

            //validate
            plan.Car = newTravelPlan.Car;
            plan.Travelers = newTravelPlan.Travelers;
        }

        public static void EditTravelPlan(int travelPlanId, DateTime startDateTime, DateTime endDateTime, string startLocation, string endLocation, Car car, List<Employee> travelers)
        {
            TravelPlan plan = TravelPlans.Find(p => p.Id.Equals(travelPlanId));
            plan.StartDateTime = startDateTime;
            plan.EndDateTime = endDateTime;
            plan.StartLocation = startLocation;
            plan.EndLocation = endLocation;

            //validate
            plan.Car = car;
            plan.Travelers = travelers;
        }

        public static List<Car> GetAllCars()
        {
            return Cars;    // maybe not needed
        }

        public static List<Car> GetAvailbleCars(DateTime startDateTime, DateTime endDateTime)
        {
            List<TravelPlan> tempPlans = new List<TravelPlan>();

            foreach (var item in TravelPlans)
            {
                tempPlans.Add(item);
            }

            List<Car> UnavailableCars = tempPlans
                .Where(
                    p => p.StartDateTime.IsBetween(startDateTime, endDateTime) &&
                    p.EndDateTime.IsBetween(startDateTime, endDateTime))
                .Select(p => p.Car)
                .ToList();

            List<Car> AvailableCars = Cars.Except(UnavailableCars).ToList();

            foreach (Car c in AvailableCars)
            {
                Console.WriteLine(c.Type);
            }

            return AvailableCars;

            /*return TravelPlans
                .Where(
                    p => p.StartDateTime.IsNotBetween(startDateTime, endDateTime) &&
                    p.EndDateTime.IsNotBetween(startDateTime, endDateTime) == false)
                .Select(p => p.Car)
                .ToList();*/
        }

        public static List<Car> GetAvailbleCars(DateTimeRange timeSpan)
        {
            List<Car> UnavailableCars = GetTravelPlans()
                .Where(
                    p => p.StartDateTime.IsBetween(timeSpan.Start, timeSpan.End) &&
                    p.EndDateTime.IsBetween(timeSpan.Start, timeSpan.End))
                .Select(p => p.Car)
                .ToList();

            List<Car> AvailableCars = Cars.Except(UnavailableCars).ToList();

            return AvailableCars;
        }

        public static Car GetCar(string plates)
        {
            return Cars.Find(c => c.Plates == plates);
        }

        public static Employee GetEmployee(int id)
        {
            return Employees.Find(e => e.EmployeeID == id);
        }

        public static List<Employee> GetEmployees()
        {
            return Employees;
        }

        public static TravelPlan GetTravelPlan(int id)
        {
            return TravelPlans.Find(t => t.Id == id);
        }

        public static List<TravelPlan> GetTravelPlans()
        {
            return TravelPlans;
        }

        public static List<TravelPlan> GetTravelPlans(int year, int month)
        {
            return TravelPlans
                    .Where(t => t.StartDateTime.Year == year && t.StartDateTime.Month == month)
                    .ToList();
        }

        public static List<TravelPlan> GetTravelPlansInMonth(int month)
        {
            /*if (month < 1 && month > 12)
            {
                return new List<TravelPlan>();
            }*/

            return TravelPlans.Where(t => t.StartDateTime.Month == month).ToList();
        }

        public static List<Employee> GetAvailableEmployees(DateTime startDateTime, DateTime endDateTime)
        {
            List<Employee> UnavailableTravelers = TravelPlans
                .Where(
                    e => e.StartDateTime.IsBetween(startDateTime, endDateTime) &&
                    e.EndDateTime.IsBetween(startDateTime, endDateTime))
                .SelectMany(e => e.Travelers)
                .ToList();

            List<Employee> AvailableEmployees = Employees.Except(UnavailableTravelers).ToList();

            return AvailableEmployees;
        }

        /********************************************/
        public static void CreateCar(Car car)
        {
            Cars.Add(car);
        }

        public static List<Car> getUsedCars()
        { 
            return TravelPlans.Select(tp => tp.Car).ToList();
        }

        public static List<Car> getUsedCars(int startYear, int startMonth, int startDay, int startHour, int startMinutes, int endYear, int endMonth, int endDay, int endHour, int endMinutes)
        {
            DateTime start = new DateTime(startYear, startMonth, startDay, startHour, startMinutes, 0, DateTimeKind.Local);
            DateTime end = new DateTime(endYear, endMonth, endDay, endHour, endMinutes, 0, DateTimeKind.Local);
            
            return TravelPlans
                    .Where(
                        tp =>
                            tp.StartDateTime.IsBetween(start, end) &&
                            tp.EndDateTime.IsBetween(start, end))
                    .Select(tp => tp.Car)
                    .ToList();
        }

        public static List<Car> getUsedCars(CustomDateTime from, CustomDateTime to)
        {
            return TravelPlans
                    .Where(
                        tp =>
                            tp.StartDateTime.IsBetween(from.MyDateTime, to.MyDateTime) &&
                            tp.EndDateTime.IsBetween(from.MyDateTime, to.MyDateTime))
                    .Select(tp => tp.Car)
                    .ToList();

            /*Car c = new Car("ZG", "Test", "Bugatti Divo", "Blue", 5);
            List<Car> cl = new List<Car>();
            cl.Add(c);
            return cl;*/
        }

        /*public static List<Car> getUsedCars(DateTime from, DateTime to)
        {
            return TravelPlans
                    .Where(
                        tp =>
                            tp.StartDateTime.IsBetween(from, to) &&
                            tp.EndDateTime.IsBetween(from, to))
                    .Select(tp => tp.Car)
                    .ToList();
        }*/

        public static List<Car> GetAvailbleCars22222(CustomDateTime from, CustomDateTime to)
        {
            List<Car> UnavailableCars = TravelPlans
                .Where(
                    p => p.StartDateTime.IsBetween(from.MyDateTime, to.MyDateTime) &&
                    p.EndDateTime.IsBetween(from.MyDateTime, to.MyDateTime))
                .Select(p => p.Car)
                .ToList();

            List<Car> AvailableCars = Cars.Except(UnavailableCars).ToList();

            return AvailableCars;
        }

        public static List<Car> GetAvailbleCars(CustomDateTime from, CustomDateTime to)
        {
            List<Car> copy = new List<Car>();
            Cars.ForEach(c => copy.Add(c));

            List<Car> notAvailable = TravelPlans
                .Where(tp =>
                    tp.StartDateTime.IsBetween(from.MyDateTime, to.MyDateTime) &&
                    tp.EndDateTime.IsBetween(from.MyDateTime, to.MyDateTime) &&
                    (from.MyDateTime < tp.StartDateTime && to.MyDateTime > tp.EndDateTime))
                    .Select(tp => tp.Car)
                    .Distinct()
                    .ToList();

            foreach (Car car in notAvailable)
            {
                copy.Remove(car);
            }

            return copy;
        }

        public static List<Employee> GetAvailableEmployees(CustomDateTime from, CustomDateTime to)
        {
            HashSet<Employee> UnavailableEmployees = TravelPlans
                .Where(
                    e => e.StartDateTime.IsNotBetween(from.MyDateTime, to.MyDateTime) &&
                    e.EndDateTime.IsNotBetween(from.MyDateTime, to.MyDateTime))
                .SelectMany(e => e.Travelers)
                .ToHashSet();

            List<Employee> AvailableEmployees = Employees.Except(UnavailableEmployees.ToList()).ToList();

            return AvailableEmployees.ToList();
        }

        /*public static List<Employee> GetAvailableEmployees(CustomDateTime from, CustomDateTime to)
        {
            List<Employee> UnavailableTravelers = TravelPlans
                .Where(
                    e => e.StartDateTime.IsBetween(from.MyDateTime, to.MyDateTime) &&
                    e.EndDateTime.IsBetween(from.MyDateTime, to.MyDateTime))
                .SelectMany(e => e.Travelers)
                .ToList();

            List<Employee> AvailableEmployees = Employees.Except(UnavailableTravelers).ToList();

            return AvailableEmployees;
        }*/
    }
}
