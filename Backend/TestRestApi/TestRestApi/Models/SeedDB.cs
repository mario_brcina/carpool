﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestRestApi.Models
{
    public static class SeedDB
    {
        public static List<Employee> InitEmployees()
        {
            return new List<Employee>
            {
                new Employee("John Doe", true),
                new Employee("Bill Gates", false),
                new Employee("Steve Jobs", false),
                new Employee("Elon Musk", true),
                new Employee("Nikola Tesla", false),
                new Employee("Michael Schumacher", true),
                new Employee("Sebastien Loeb", true),
                new Employee("Lewis Hamilton", true),
                new Employee("Sebastian Vettel", true),
                new Employee("Satya Nadella", false),
                new Employee("James Gosling", false),
                new Employee("Guido van Rossum", false),
                new Employee("Bjarne Stroustrup", false),
                new Employee("Larry Page", false),
                new Employee("Sergey Brin", false),
                new Employee("James Bond", true),
                new Employee("Berenice Marlohe", false),
                new Employee("Monica Bellucci", false),
                new Employee("Angelina Jolie", true),
                new Employee("Ursula Andress", true),
                new Employee("Marilyn Monroe", false),
                new Employee("Sharon Stone", false),
                new Employee("Megan Fox", true),
                new Employee("Emily Ratajkowski", false),
                new Employee("Salma Hayek", false)
            };
        }

        public static List<Car> InitCars()
        {
            return new List<Car>
            {
                new Car("ZG-100-PU", "GTC4Lusso", "Ferrari GTC4Lusso", "Red", 4),
                new Car("ZG-101-PU", "Panamera", "Porsche Panamera", "Grey", 4),
                new Car("ZG-102-PU", "Rover", "Range Rover", "Dark Blue", 5),
                new Car("ZG-103-PU", "Bimmer", "BMW M5", "Blue", 5),
                new Car("ZG-104-PU", "Merc", "Mercedes E63AMG", "Dark Grey", 5),
                new Car("ZG-105-PU", "Bond's Car", "Aston Martin one 77", "Silver", 2),
                new Car("ZG-106-PU", "Sporty Avant", "Audi RS4", "Dark Green", 5),
                new Car("ZG-107-PU", "Passat", "VW Passat", "Black", 5),
                new Car("ZG-108-PU", "Skoda", "Skoda Octavia", "Grey", 5),
                new Car("ZG-109-PU", "Van", "Mercedes-Benz Vito", "Black", 7)
            };
        }
    }
}
