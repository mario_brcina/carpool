﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestRestApi.Models
{
    public class CustomDateTime
    {
        public DateTime MyDateTime { get; set; }

        public CustomDateTime(string csd)
        {
            string[] dateTimeParts = csd.Split('-');

            int[] parts =
            {
                int.Parse(dateTimeParts[0]),
                int.Parse(dateTimeParts[1]),
                int.Parse(dateTimeParts[2]),
                int.Parse(dateTimeParts[3]),
                int.Parse(dateTimeParts[4]),
            };

            this.MyDateTime = new DateTime(parts[0], parts[1], parts[2], parts[3], parts[4], 0);
        }

        public override string ToString()
        {
            return this.MyDateTime.ToString();
        }
    }
}
