﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestRestApi.Models
{
    public class Employee
    {
        private static int id = 0;
        public int EmployeeID { get; set; }
        public string Name { get; set; }
        public bool IsDriver { get; set; }

        public Employee(string name, bool isDriver)
        {
            id += 1;
            this.EmployeeID = id;
            this.Name = name;
            this.IsDriver = isDriver;
        }

        public override string ToString()
        {
            return String.Format($"ID: {EmployeeID}, Name: {Name}, Is Driver: {IsDriver}");
        }
    }
}
