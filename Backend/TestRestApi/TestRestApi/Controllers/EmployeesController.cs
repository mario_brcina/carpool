﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TestRestApi.Models;

namespace TestRestApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeesController : ControllerBase
    {
        //IRepository repo = Repository.Instance;

        [HttpGet]
        public IEnumerable<Employee> GetEmployees()
        {
            return Repository.GetEmployees();
        }

        [HttpGet("{startDateTime}/{endDateTime}")]
        public IEnumerable<Employee> Get(string startDateTime, string endDateTime)
        {
            CustomDateTime from = new CustomDateTime(startDateTime);
            CustomDateTime to = new CustomDateTime(endDateTime);
            return Repository.GetAvailableEmployees(from, to);
        }
    }
}