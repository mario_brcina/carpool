﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TestRestApi.Models;

namespace TestRestApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TravelPlansController : ControllerBase
    {
        //IRepository repo = Repository.Instance;

        [HttpGet]
        public List<TravelPlan> Get()
        {
            return Repository.GetTravelPlans();
        }

        [HttpGet("{month}", Name = "Get")]
        public List<TravelPlan> Get(int month)
        {
            return Repository.GetTravelPlansInMonth(month);
        }

        [HttpGet("{year}/{month}", Name = "GetTravelPlans")]
        public List<TravelPlan> Get(int year, int month)
        {
            return Repository.GetTravelPlans(year, month);
        }

        [HttpPost]
        public void Post([FromBody] TravelPlan plan)
        {
            Repository.CreateTravelPlan(plan);
        }

        [HttpPut("{id}")]
        public void Put(int id, [FromBody]TravelPlan plan)
        {
            Repository.EditTravelPlan(id, plan);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            Repository.DeleteTravelPlan(id);
        }
    }
}