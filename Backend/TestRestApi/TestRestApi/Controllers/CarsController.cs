﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TestRestApi.Models;

namespace TestRestApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CarsController : ControllerBase
    {
        //IRepository repo = Repository.Instance;

        [HttpGet]
        public IEnumerable<Car> GetCars()
        {
            return Repository.GetAllCars(); 
        }

        [HttpGet("{startDateTime}/{endDateTime}")]
        public IEnumerable<Car> Get(string startDateTime, string endDateTime)
        {
            CustomDateTime from = new CustomDateTime(startDateTime);
            CustomDateTime to = new CustomDateTime(endDateTime);
            return Repository.GetAvailbleCars(from, to);
        }

        [HttpPost]
        public void Post([FromBody] Car car)
        {
            Repository.CreateCar(car);
        }










        /*[HttpGet("{from}/{to}", Name = "GetCars")]
        public IEnumerable<Car> Get(DateTime from, DateTime to)
        {
            return repo.GetAvailbleCars(from, to);
        }*/

        /*[HttpGet("/available")]
        public IEnumerable<Car> Get([FromBody] DateTimeRange timeSpan)
        {
            if (timeSpan == null || timeSpan.Start == null || timeSpan.End == null)
            {
                return repo.GetAllCars();
            }
            return repo.GetAvailbleCars(timeSpan);
        }*/



        /*[HttpGet]
        public IEnumerable<Car> GetCars(int? startYear, int? startMonth, int? startDay, int? startHour, 
            int? startMinutes, int? endYear, int? endMonth, int? endDay, int? endHour, int? endMinutes)
        {
            if (startYear == null || startMonth == null || startDay == null || startHour == null
                || startMinutes == null || endYear == null || endMonth == null || endDay == null
                || endHour == null || endMinutes == null)
            {
                return Repository.GetAllCars(); 
            }else
            {

                return Repository.GetAvailbleCars(
                    new DateTime(startYear.Value, startMonth.Value, startDay.Value, startHour.Value, startMinutes.Value, 0),
                    new DateTime(endYear.Value, endMonth.Value, endDay.Value, endHour.Value, endMinutes.Value, 0));
            }
        }*/
    }
}
 