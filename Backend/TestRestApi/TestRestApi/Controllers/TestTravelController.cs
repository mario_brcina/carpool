﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TestRestApi.Models;

namespace TestRestApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestTravelController : ControllerBase
    {


        /*HttpGet]
        public IEnumerable<Car> Get()
        {
            return Repository.getUsedCars();
        }*/

        [HttpGet("{startDateTime}/{endDateTime}")]
        public IEnumerable<Car> Get(string startDateTime, string endDateTime)
        {
            CustomDateTime from = new CustomDateTime(startDateTime);
            CustomDateTime to = new CustomDateTime(endDateTime);
            return Repository.getUsedCars(from, to);
        }

        /*[HttpGet]
        public IEnumerable<Car> Get([FromBody] DateTimeSpan span)
        {
            
            return Repository.getUsedCars(span.From, span.To);
        }*/

    }
}