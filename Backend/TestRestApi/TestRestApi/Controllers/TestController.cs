﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TestRestApi.Models;

namespace TestRestApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {

        [HttpGet]
        public IEnumerable<Car> Get()
        {
            return Repository.getUsedCars();
        }

        [HttpGet("{startYear}/{startMonth}/{startDay}/{startHour}/{startMinutes}/{endYear}/{endMonth}/{endDay}/{endHour}/{endMinutes}")]
        public IEnumerable<Car> Get(int startYear, int startMonth, int startDay, int startHour, int startMinutes, int endYear, int endMonth, int endDay, int endHour, int endMinutes)
        {
            
                return Repository.getUsedCars(startYear, startMonth, startDay, startHour, startMinutes, endYear, endMonth, endDay, endHour, endMinutes);      
        }
    }
}